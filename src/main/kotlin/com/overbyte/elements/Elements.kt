package com.overbyte.elements

/**
 * Root type for all other elements of the program
 */
interface Element

/**
 * Atomic type. Represents the base abstraction for things like ints and floats
 */
interface AtomicType : Element

/**
 * User Type. Represents a user type in a declaration. Though this is subtle, name
 * will be the name of the type (ie; Person) while type will refer to the instance of
 * the Person object
 */
class UserType(val name: String) : Element {
    private var _type: Element? = null
    var type: Element?
        get() = _type
        set(value) {
            _type = value
        }

    override fun toString(): String {
        return "$type"
    }
}

/**
 * Int Type. Represents an atomic int
 */
object IntType : AtomicType {
    override fun toString(): String = "int"
}

/**
 * Float Type. Represents an atomic float
 */
object FloatType : AtomicType {
    override fun toString(): String = "float"
}

/**
 * Date Type. Represents an atomic date
 */
object DateType : AtomicType {
    override fun toString(): String = "date"
}

/**
 * String Type. Represents an atomic string
 */
class StringType(val length: Int) : AtomicType {
    override fun toString(): String = "varchar[$length]"
}

/**
 * Seq Type. Represents an atomic sequence of some element type
 */
class SeqType(val type: Element) : AtomicType {
    override fun toString(): String = "seq[$type]"
}

/**
 * Represents an identifier and its associated type (age: int)
 */
class Declaration(val name: String, val type: Element) : Element {
    override fun toString(): String {
        return "$name: $type"
    }
}

/**
 * Represents a type that has one or more possible states
 * NOTE - These states are not defined by any sort of value yet
 */
class Enumeration(val name: String, val values: List<String>) : Element {
    override fun toString(): String {
        return "(Enum : $name) ${values.reduce { x, y -> "$x, $y"}}" // Same as mkString in Scala
    }
}

/**
 * Represents a user defined type with one or more declarations
 */
class Entity(val name: String, val declarations: List<Declaration>) : Element {
    override fun toString(): String {
        return """
(Entity : $name)
${declarations.map {"   $it\n"}}
            """
    }
}

/**
 * Represents a combination of one or more elements, typically enums and entities. All
 * generators are given an instance of this class, as created by the parser.
 * NOTE - In order to improve this, we may want to introduce the interface TopLevelElement
 */
class Program(var items: List<Element>) : Element {
    override fun toString(): String {
        return "${items.map { it.toString() }}"
    }
}
