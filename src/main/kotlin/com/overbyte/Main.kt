package com.overbyte

import com.github.h0tk3y.betterParse.combinators.*
import com.github.h0tk3y.betterParse.grammar.Grammar
import com.github.h0tk3y.betterParse.grammar.parseToEnd
import com.github.h0tk3y.betterParse.grammar.parser
import com.github.h0tk3y.betterParse.parser.Parser
import com.overbyte.elements.*

object CrudgenGrammar : Grammar<Program>() {
    private val colon by token(":")
    private val comma by token(",")
    private val lcurly by token("""\{""")
    private val rcurly by token("""\}""")

    private val qmark by token("""\?""")
    private val enum by token("""enum\b""")
    private val entity by token("""entity\b""")
    private val varchar by token("""varchar\b""")
    private val lbracket by token("""\[""")
    private val rbracket by token("""\]""")
    private val float by token("""float\b""")
    private val int by token("""int\b""")
    private val date by token("""date\b""")
    private val seq by token("""seq\b""")

    // IntelliJ might say that ws is not used, but don't be tempted to delete it. If you want things to work, this
    // is crucial!
    private val ws by token("""\s+""", ignore = true)
    //private val newline by token("""[\r\n]+""", ignore = true)
    private val floatLiteral by token("""\d+\.\d+""")
    private val intLiteral by token("""\d+""")
    private val ident by token("""([a-zA-Z][a-zA-Z0-9_]*)""")

    private val userTypes: MutableMap<String, Element> = HashMap()
    private val userTypeDeclarations: MutableList<UserType> = mutableListOf()

    // The following is using parser. Why do we need it? Well, without it, the declaration for enumBody would have to appear
    // before enumParser. Not sure I like this, as it violates the Principle of Least Surprise.
    private val enumParser = -enum and ident and -lcurly and parser { enumBody } and -rcurly map { (name, enumerants) ->
        val result = Enumeration(name.text, enumerants)
        userTypes[name.text] = result
        result
    }

    private val enumBody = separated(ident, comma) map  { it.terms.map { it.text } }

    private val entityParser = -entity and ident and -lcurly and parser { entityBody } and -rcurly map { (name, definitions) ->
        val result = Entity(name.text, definitions)
        userTypes[name.text] = result
        result
    }

    private val entityBody = oneOrMore(parser { declaration })

    private val intParser = int use { IntType }

    private val dateParser = date use { DateType }

    private val varcharParser = -varchar and -lbracket and intLiteral and -rbracket map { StringType(it.text.toInt()) }

    private val floatParser = float use { FloatType }

    private val sequenceParser = -seq and -lbracket and parser { declarationType } and -rbracket map { SeqType(it)}

    // TODO - Not quite finished. Need to build the type
    private val userTypeParser = ident map {
        val result = UserType(it.text)
        userTypeDeclarations.add(result)
        result
    }

    private val declarationType: Parser<Element> = intParser or dateParser or varcharParser or floatParser or sequenceParser or userTypeParser

    private val declaration: Parser<Declaration> = ident and -colon and declarationType map { (name, defType) -> Declaration(name.text, defType) }

    private val topLevel: Parser<Element> = enumParser or entityParser

    override val rootParser: Parser<Program> =
        oneOrMore(topLevel) map {
            // At this point, we should be able to link the user defined types to any declarations that may use them
            for (item in userTypeDeclarations) {
                if (userTypes[item.name] != null) {
                    item.type = userTypes[item.name]
                }
                else {
                    throw Exception("Encountered an unknown user type '$item.name'")
                }
            }
            Program(it)
        }

}

fun main(args: Array<String>) {
    val code2 = """
        enum Nursery {
            Mary, Had,
            Little,
            Lamb,
            Nimble,
            Quick
        }
        enum Gender { Female, Male }
        entity Address {
            street: varchar[30]
            city: varchar[20]
            state: varchar[2]
            postalCode: varchar[13]
        }
        entity Person {
            firstName : varchar[20]
            lastName : varchar[20]
            dateOfBirth : date
            address : Address
        }
        entity Business {
            name: varchar[100]
            location: Address
            employees: seq[Person]
        }
        """


    val program: Program = CrudgenGrammar.parseToEnd(code2)
    println("The user types defined were $program")
}

