package com.overbyte.plugins.database

import com.overbyte.plugins.Plugin

/**
 *
 * Database Plugins
 *
 */

interface DatabasePlugin : Plugin

class MySqlPlugin : DatabasePlugin

class PostgresPlugin : DatabasePlugin

class SqlitePlugin : DatabasePlugin

