package com.overbyte.plugins.backEnd.php

import com.overbyte.plugins.backEnd.BackEndPlugin

open class PhpPlugin : BackEndPlugin

class PhpServicePlugin : PhpPlugin()

class PhpTestPlugin : PhpPlugin()

class PhpDaoPlugin : PhpPlugin()


