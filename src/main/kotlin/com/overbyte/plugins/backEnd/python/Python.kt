package com.overbyte.plugins.backEnd.python

import com.overbyte.plugins.backEnd.BackEndPlugin

open class PythonPlugin : BackEndPlugin

class PythonServicePlugin : PythonPlugin()

class PythonTestPlugin : PythonPlugin()

class PythonDaoPlugin : PythonPlugin()

