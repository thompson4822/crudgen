package com.overbyte.plugins.backEnd.kotlin

import com.overbyte.plugins.backEnd.BackEndPlugin

open class KotlinPlugin : BackEndPlugin

class KotlinServicePlugin : KotlinPlugin()

class KotlinTestPlugin : KotlinPlugin()

class KotlinDaoPlugin : KotlinPlugin()

