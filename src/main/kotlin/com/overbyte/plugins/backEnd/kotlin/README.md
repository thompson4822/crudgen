##A Note About The Backend
While there are plenty of good targets out there, I wanted something that would be fast and simple to get started with. Because of this, I've chosen **Javalin**, but may one day switch to something more marketable like **Spring Boot**.
