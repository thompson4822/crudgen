package com.overbyte.plugins.backEnd.node

import com.overbyte.plugins.backEnd.BackEndPlugin

open class NodePlugin : BackEndPlugin

class NodeServicePlugin: NodePlugin()

class NodeTestPlugin : NodePlugin()

class NodeDaoPlugin : NodePlugin()


