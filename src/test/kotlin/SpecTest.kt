import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.*
import java.util.*

class StringTest : FunSpec() {
    init {
        test("String.startsWith should be true for a prefix") {
            "helloworld".startsWith("hello") shouldBe true
        }
    }
}

class MyTestsWord : WordSpec() {
    init {
        "String.length" should {
            "return the length of the string" {
                "hello".length shouldBe 5
                "".length shouldBe 0
            }
        }
    }
}

// I like this form of testing. Seems like the default I'll probably use
class MyTestsShould : ShouldSpec() {
    init {
        "Strings.length" {
            should("return the length of the string") {
                "hello".length shouldBe 5
            }
            should("support empty strings") {
                "".length shouldBe 0
            }
        }
    }
}

// Note that an item had to be pushed to the stack first in order for the following
// tests to work properly
class MyFeatureSpec : FeatureSpec() {
    init {
        feature("a stack") {
            val stack = Stack<String>()
            stack.push("Kotlin")
            scenario("should be non-empty when an item is pushed") {
                stack.isEmpty() shouldBe false
            }
            scenario("should be empty when the item is popped") {
                stack.pop()
                stack.isEmpty() shouldBe true
            }
        }
    }
}

class MyStringSpec : StringSpec() {
    init {
        "strings.length should return the size of the string" {
            "hello".length shouldBe 5
        }
    }
}