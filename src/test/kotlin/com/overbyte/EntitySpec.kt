package com.overbyte

import com.github.h0tk3y.betterParse.grammar.parseToEnd
import com.overbyte.CrudgenGrammar
import io.kotlintest.specs.ShouldSpec

class EntitySpec : ShouldSpec() {
    val code = """
        enum Gender { Female, Male }
        entity Address {
            street : varchar[30]
            city: varchar[20]
            state : varchar[2]
            postalCode: varchar[13]
        }
        entity Person {
            firstName: varchar[20]
            lastName: varchar[20]
            address: Address
            gender: Gender
            dateOfBirth: date
            income: float
            iq: int
        }
        """

    val program = CrudgenGrammar.parseToEnd(code)

    val empty = "entity Empty { }"

    init {
        "Parsing entities" {
            should("handle a multi-line declaration") {
//                program.items.size shouldBe 1
//                val firstElement = program.items.last()
//                if (firstElement is Enumeration) {
//                    firstElement shouldNotBe null
//                    firstElement.values.last() shouldBe "Unknown"
//                }
            }
       }

    }
}