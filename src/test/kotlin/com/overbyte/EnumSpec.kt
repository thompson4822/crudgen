package com.overbyte

import com.github.h0tk3y.betterParse.grammar.parseToEnd
import com.github.h0tk3y.betterParse.parser.ParseException
import com.overbyte.elements.Enumeration
import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldNotBe
import io.kotlintest.matchers.shouldThrow
import io.kotlintest.specs.*

class EnumSpec : ShouldSpec() {
    val genders = """
        enum Gender {
            Female,
            Male, Unknown
        }
        """

    val colors = "enum Color { Red, Green, Purple, White }"

    val empty = "enum Empty { }"

    init {
        "Parsing enumerations" {
            should("handle a multi-line declaration") {
                val program = CrudgenGrammar.parseToEnd(genders)
                program.items.size shouldBe 1
                val firstElement = program.items.last()
                if(firstElement is Enumeration) {
                    firstElement shouldNotBe null
                    firstElement.values.last() shouldBe "Unknown"
                }
            }
            should("handle a single line declaration") {
                val program = CrudgenGrammar.parseToEnd(colors)
                program.items.size shouldBe 1
                val firstElement = program.items.last()
                if(firstElement is Enumeration) {
                    firstElement shouldNotBe null
                    firstElement.values[0] shouldBe "Red"
                }

            }
            should("reject an empty declaration") {
                shouldThrow<ParseException> {
                    CrudgenGrammar.parseToEnd(empty)
                }
            }
        }
    }
}

