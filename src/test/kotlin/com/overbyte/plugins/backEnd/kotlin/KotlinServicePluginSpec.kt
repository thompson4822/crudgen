package com.overbyte.plugins.backEnd.kotlin

import com.github.h0tk3y.betterParse.grammar.parseToEnd
import com.overbyte.CrudgenGrammar
import io.kotlintest.specs.ShouldSpec

class KotlinServicePluginSpec : ShouldSpec() {
    val code = """
        enum Gender { Female, Male, Unknown }
        entity Address {
            street : varchar[30]
            city: varchar[20]
            state : varchar[2]
            postalCode: varchar[13]
        }
        entity Person {
            firstName: varchar[20]
            lastName: varchar[20]
            address: Address
            gender: Gender
            dateOfBirth: date
            income: float
            iq: int
        }
        """

    val program = CrudgenGrammar.parseToEnd(code)

    // TODO - The following need to be corrected
    init {
        "Generating Service" {
            should("create appropriate files") {
                // With the definition above, there should be the files
                // AddressDao and PersonDao
            }.config(enabled = false)

            should("provide a create method") {
                // Any Dao generated should have a set method
            }.config(enabled = false)

            should("provide a read method") {
                // Any Dao generated should have a get method
            }.config(enabled = false)

            should("provide a update method") {
                // Any Dao generated should have a update method
            }.config(enabled = false)

            should("provide a delete method") {
                // Any Dao generated should have a delete method
            }.config(enabled = false)

            should("provide a read method for any declarations that are user types") {
                // All fields that are user types (ie; address : Address) must provide a read method (getAddress)
            }.config(enabled = false)

            should("provide a readAll method for any declarations that are of type seq") {
                // If declaration is a seq (ie; employees : seq[Person] must provide a readAll method (getAllEmployees)
            }.config(enabled = false)
        }

    }
}